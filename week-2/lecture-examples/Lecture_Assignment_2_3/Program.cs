﻿using System;

namespace Lecture_Assignment_2_3
{
    class Program
    {
        static void Main(string[] args)
        {
            UniqueLetters("Johannes Kantola");
            NOfUniqueLetters("Johannes Kantola");

            // 1
            void UniqueLetters(string input)
            {
                string checkedChars = "";

                foreach(char c in input)
                {
                    if (!checkedChars.Contains(c))
                    {
                        checkedChars += c;
                        Console.WriteLine(c);
                    }
                }
            }

            // 2
            void NOfUniqueLetters(string input)
            {
                string checkedChars = "";

                foreach (char c in input)
                {
                    if (!checkedChars.Contains(c))
                    {
                        checkedChars += c;
                    }
                }

                foreach (char c in checkedChars)
                {
                    int nOfCurrent = 0;
                    foreach (char ch in input)
                    {
                        if (ch == c)
                            nOfCurrent++;
                    }
                    Console.WriteLine($"Number of {c}'s: {nOfCurrent}");
                }

            }
        }
    }
}
