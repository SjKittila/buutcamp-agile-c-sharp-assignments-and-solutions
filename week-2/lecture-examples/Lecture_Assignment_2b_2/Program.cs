﻿using System;

namespace Lecture_Assignment_2b_2
{
    class Animal
    {
        private string name;
        private string sound;

        public Animal(string animalName, string animalSound)
        {
            name = animalName;
            sound = animalSound;
        }

        public void Greet()
        {
            Console.WriteLine($"{name} says {sound}!");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Animal dog = new Animal("Dog", "Woof");
            Animal cat = new Animal("Cat", "Meow");

            dog.Greet();
            cat.Greet();
        }
    }
}
