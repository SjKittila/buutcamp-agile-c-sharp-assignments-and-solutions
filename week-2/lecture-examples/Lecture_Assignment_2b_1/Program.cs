﻿using System;
using System.Collections.Generic;

namespace Lecture_Assignment_2b_1
{
    class User
    {
        public string userName;
        public string passWord;
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<User> userList = new List<User>();
            while (true)
            {
                User user = new User();

                Console.Write("Username: ");
                string userName = Console.ReadLine();
                Console.Write("Password: ");
                string password = Console.ReadLine();

                user.userName = userName;
                user.passWord = password;

                userList.Add(user);

                foreach(User u in userList)
                {
                    Console.WriteLine(u.userName);
                }
            }
        }
    }
}
