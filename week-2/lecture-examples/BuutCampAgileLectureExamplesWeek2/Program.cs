﻿using System;

namespace BuutCampAgileLectureExamplesWeek2
{
    class Program
    {
        static void Main(string[] args)
        {
            Sum(10, 2);
            Sum(-10, 3);
            Difference(-100, -100);
            Difference(10, 10);

            void Sum(int value1, int value2)
            {
                int sum = value1 + value2;
                Console.WriteLine($"The sum of {value1} and {value2} is {sum}");
            }
            void Difference(int value1, int value2)
            {
                int difference = value1 - value2;
                Console.WriteLine($"The difference of {value1} and {value2} is {difference}");
            }
            
        }
    }
}
