﻿using System;

namespace Lecture_Assignment_2_2
{
    class Program
    {
        static void Main(string[] args)
        {
            CountSpaces("asdf oaisjdfpoais poaisdjgao aposidjfaos poisjd asdf");
            Console.WriteLine(
                RemoveSpaces("The quick brown fox jumped over the lazy dog"));

            // 1
            void CountSpaces(string text)
            {
                int nOfSpaces = 0;
                foreach(char c in text)
                {
                    if(c == ' ')
                    {
                        nOfSpaces++;
                    }
                }
                Console.WriteLine(nOfSpaces);
            }

            // 2
            string RemoveSpaces(string input)
            {
                string output = "";

                foreach(char c in input)
                {
                    if(c != ' ')
                    {
                        output += c;
                    }
                }

                return output;
            }
        }
    }
}
