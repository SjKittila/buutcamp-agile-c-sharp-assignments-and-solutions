﻿using System;
using System.Collections.Generic;

namespace Lecture_Assignment_2b_3
{
    class Animal
    {
        public string name;
        public string sound;
        public void Greet()
        {
            Console.WriteLine($"{name} says {sound}!");
        }
    }
    class Dog : Animal
    {
        public void WiggleTail()
        {
            Console.WriteLine("Wiggling tail");
        }
    }
    class Cat : Animal
    {
        public void Purr()
        {
            Console.WriteLine("Purring");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List <Animal> allAnimals = new List<Animal>();

            Animal kitten = new Cat();
            Animal puppy = new Dog();

            kitten.name = "Whiskers";
            kitten.sound = "Meow";
            puppy.name = "Spotty";
            puppy.sound = "Woof";

            allAnimals.Add(kitten);
            allAnimals.Add(puppy);

            foreach(Animal animal in allAnimals)
            {
                animal.Greet();
            }

            Cat whiskers = (Cat)kitten;
            whiskers.Purr();
        }
    }
}
