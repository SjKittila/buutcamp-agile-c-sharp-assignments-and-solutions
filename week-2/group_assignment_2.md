## Group Assignment 2: Rock Paper Scissors

### Introduction - Git

One team member should create a gitlab repository for this week’s group assignment. The rest of the team should have access to the repository too.

Every task should be handled as a new feature.
This means that whenever a new task is started, a new branch is created for it (`git checkout -b feature-name`).

Once the task has been completed, it should be committed (`git add .`; `git commit -m "Descripe changes"`).

At this point, the branch itself can be pushed to the remote repository (`git push --set-upstream origin feature-name`), so that the other team members can see the changes. It’s a good habit to have the team agree that the changes look ok before they are applied to the master branch.

If the team thinks that the new changes should be adjusted, any necessary modifications can be done, committed and pushed to the remote repository again for re-evaluation.

Once the team has okayed the changes, they can be merged into the master branch (`git checkout master`; `git merge feature-name`).

After that the commit(s) should be pushed upstream, to the remote repository (`git push`).

And after pushing the changes, the rest of the team can pull the new commit(s) (git pull).

### Introduction - Rock Paper Scissors

The goal this week is to code a piece of software that can evaluate Rock Paper Scissor (RPS) hands against each other.
To start, one of the group members should create a new C# console project and push it to gitlab. The name should be among the lines of “Rock Paper Scissors”. The rest of the team should then clone the project and verify that it compiles and runs without problems.

## Mandatory

### G2.0. Necessary types

Create `Hand.cs` and `Winner.cs` files for these types:
    public enum Hand { Rock, Paper, Scissors }
    public enum Winner { Player1, Player2, Draw }

The type definitions should happen in the project namespace.

### G2.1. Rock Paper Scissors: Round

Create a new class called `Round`.
The purpose of Round is to keep track of a single RPS round.
It needs to offer some way for the other code to give it two Hand variables and return the Winner accordingly (Rock beats Scissors, Scissors beat Paper, Paper beats Rock).

Create an instance of the `Round` class in the main method to test it. Try passing different hands to it and print the winner using Console.WriteLine method.

Hint 1:
You can make the class constructor `Round` take the two `Hand` parameters and save the `Winner` for later retrieval.
The constructor signature could look like this:
    public Round(Hand player1, Hand player2);
    
Hint 2:
If you follow the first hint, you can use the class like this:
    Winner winner = new Round(Hand.Paper, Hand.Rock).RoundWinner;

### G2.2. Rock Paper Scissors: Multiple rounds

In the main method create a variable with the type of `List<Round>`. The purpose of the variable is to contain multiple `Round` objects so that we can handle them neatly.
Use the `Add` method provided by the new variable to add new Round objects into it.
Finally, after there are multiple `Round` objects stored into the list, have the program loop over the rounds and print who won more of the rounds.

## Recommended

### G2.3. Rock Paper Scissors: Random

Use the RandomNumberGenerator in a loop to automatically generate rounds between two imaginary players and store them into the list of rounds.
Finally modify the code so that it prints the entire round history with the played hands and the description of who won each round.

Hint:
You can create a `List<Hand>` hands variable and have it contain all three Hand values. With that, you can generate a random Hand like this:
    Hand randomHand = hands[RandomNumberGenerator.GetInt32(hands.Count)];

## Extra

### G2.4. Rock Paper Scissors: AI methods

Create a custom mehtod with the signature of Hand YourMethodName(List<Round> history). Passing the round history to it allows it to strategize upon which Hand to play.
Without using random numbers, write the necessary logic to pick a Hand and win against the opponent method. Take turns to write the AI methods and compete between the best of the pack.
