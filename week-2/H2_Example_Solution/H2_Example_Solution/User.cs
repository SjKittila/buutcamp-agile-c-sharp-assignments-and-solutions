﻿using System;
using System.Collections.Generic;
using System.Text;

namespace H2_Example_Solution
{
    class User
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public int Id { get; set; }
        public decimal Balance { get; set; }
    }
}
