﻿using System;

namespace H2_Example_Solution
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            App app = new App();
            app.Run();
        }
    }
}
