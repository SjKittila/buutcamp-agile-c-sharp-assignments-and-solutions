﻿using System;
using System.Collections.Generic;

namespace H2_Example_Solution
{
    class App
    {
        // If your User class has properties instead of variables, 
        // you can quickly initialize some dummy users into a list like this
        private List<User> allUsers = new List<User>
        {
            new User{Name = "John Doe", Password = "1234", Id = 1234, Balance = 1000},
            new User{Name = "Assi Asiakas", Password = "asdf", Id = 1000, Balance = 200},
        };

        public void Run()
        {
            Console.WriteLine("Welcome to Buutti banking CLI!");
            Console.WriteLine("Hint: You can get help with the commands by typing \"help\".");

            while (true)
            {
                Console.WriteLine("Please enter your command.");
                string command = Console.ReadLine();

                if (command == "quit")
                {
                    break;
                }

                switch (command)
                {
                    case "help":
                        HelpDialog();
                        break;

                    case "create_account":
                        CreateAccount();
                        break;

                    case "does_account_exist":
                        AccountExists();
                        break;

                    case "account_balance":
                        Balance();
                        break;

                    case "withdraw_funds":
                        Withdraw();
                        break;

                    case "deposit_funds":
                        Deposit();
                        break;

                    case "transfer_funds":
                        Transfer();
                        break;

                    default:
                        Console.WriteLine("No such command.");
                        break;
                }
            }
        }

        private void HelpDialog()
        {
            Console.WriteLine(@"
Here’s a list of commands you can use!
 
 help                  Opens this dialog.
 quit                  Quits the program.
 
 Account actions
 create_account        Opens a dialog for creating an account.
 does_account_exist    Opens a dialog for checking if an account exists.
 account_balance       Opens a dialog for logging in and prints the account balance.
 
 Fund actions
 withdraw_funds        Opens a dialog for withdrawing funds.
 deposit_funds         Opens a dialog for depositing funds.
 transfer_funds        Opens a dialog for transferring funds to another account.
                    ");
        }

        private void CreateAccount()
        {
            Console.WriteLine("Creating a new user account!");
            Console.WriteLine("Insert your name.");

            string name = Console.ReadLine();

            Console.WriteLine($"Hello, {name}! It’s great to have you as a client.");
            Console.WriteLine("How much is your initial deposit? (The minimum is 10€)");

            decimal deposit;
            do
            {
                deposit = ReadDecimal();
                if (deposit < 10)
                {
                    Console.WriteLine("Unfortunately we can’t open an account for such a small account. Do you have any more cash with you?");
                }
            } while (deposit < 10);

            Console.WriteLine($"Great, {name}! You now have an account with a balance of {deposit}.");
            Console.WriteLine("We’re happy to have you as a customer, and we want to ensure that your money is safe with us.");
            Console.WriteLine("Give us a password, which gives only you the access to your account.");

            string password = Console.ReadLine();
            int id = GenerateId();

            Console.WriteLine("Your account is now created.");
            Console.WriteLine($"Account id: {id}");
            Console.WriteLine("Store your account ID in a safe place.");

            // If your User class has properties instead of variables, 
            // you can quickly initialize a new user like this
            User user = new User { Name=name, Password=password, Id=id, Balance=deposit };
            allUsers.Add(user);
        }

        private User AccountExists()
        {
            Console.WriteLine("Checking if an account exists!");
            Console.WriteLine("Enter the account ID whose existence you want to verify.");

            User user;
            do
            {
                int id = ReadInt();
                user = GetUserById(id);

                if (user == null)
                {
                    Console.WriteLine("An account with that ID does not exist. Try again.");
                }
            } while (user == null);

            Console.WriteLine("This account exists.");
            return user;
        }

        private void Balance()
        {
            Console.WriteLine("Checking your account balance!");

            User user = ValidateUser();

            Console.WriteLine($"Your account balance is {user.Balance}€.");
        }

        private void Withdraw()
        {
            Console.WriteLine("Withdrawing cash!");

            User user = ValidateUser();

            Console.WriteLine($"How much money do you want to withdraw? (Current balance: {user.Balance}€)");

            decimal withdrawAmount;
            do
            {
                withdrawAmount = ReadDecimal();
                if (withdrawAmount > user.Balance)
                {
                    Console.WriteLine("Unfortunately you don’t have the balance for that. Try a smaller amount.");
                }
                else
                {
                    user.Balance -= withdrawAmount;
                    Console.WriteLine($"Withdrawing a cash sum of {withdrawAmount}€. Your account balance is now {user.Balance}€.");
                    break;
                }
            } while (withdrawAmount > user.Balance);
        }

        private void Deposit()
        {
            Console.WriteLine("Depositing cash!");

            User user = ValidateUser();

            Console.WriteLine($"How much money do you want to deposit? (Current balance: {user.Balance}€)");

            decimal depositAmount = ReadDecimal();
            user.Balance += depositAmount;

            Console.WriteLine($"Depositing {depositAmount}€. Your account balance is now {user.Balance}€.");
        }

        private void Transfer()
        {
            Console.WriteLine("Transferring funds!");

            User user = ValidateUser();

            Console.WriteLine($"How much money do you want to transfer? (Current balance: {user.Balance}€)");

            decimal transferAmount;
            do
            {
                transferAmount = ReadDecimal();
                if (transferAmount > user.Balance)
                {
                    Console.WriteLine("Unfortunately you don’t have the balance for that. Try a smaller amount.");
                }
            } while (transferAmount > user.Balance);

            Console.WriteLine("Which account ID do you want to transfer these funds to?");

            User recipient;
            do
            {
                int recipientId = ReadInt();
                recipient = GetUserById(recipientId);

                if (recipient == null)
                {
                    Console.WriteLine("An account with that ID does not exist. Try again.");
                }
                else
                {
                    recipient.Balance += transferAmount;
                    Console.WriteLine($"Sending {transferAmount}€ from account ID {user.Id} to account ID {recipientId}.");
                    break;
                }
            } while (recipient == null);
        }

        //////////////////////
        // Helper functions //
        //////////////////////
        
        /// <summary>
        /// Keeps requesting a value, until user inputs a decimal value greater than 0.
        /// </summary>
        private decimal ReadDecimal()
        {
            bool success = false;
            decimal input = 0;

            while (!success)
            {
                success = decimal.TryParse(Console.ReadLine(), out input);
                if (!success || input < 0)
                {
                    Console.WriteLine("Please enter a valid value.");
                    success = false;
                }
            }

            return input;
        }

        /// <summary>
        /// Keeps requesting a value, until user inputs an integer value greater than 0.
        /// </summary>
        private int ReadInt()
        {
            bool success = false;
            int input = 0;

            while (!success)
            {
                success = int.TryParse(Console.ReadLine(), out input);
                if (!success)
                {
                    Console.WriteLine("Please enter a valid value.");
                }
            }

            return input;
        }

        /// <summary>
        /// Returns a user from the allUsers list with a corresponding id.
        /// </summary>
        private User GetUserById(int id)
        {
            User foundUser = null;
            foreach (User user in allUsers)
            {
                if (user.Id == id)
                {
                    foundUser = user;
                }
            }
            return foundUser;
        }

        /// <summary>
        /// Returns a random unique ID between 0-9999.
        /// </summary>
        private int GenerateId()
        {
            Random rand = new Random();
            int id;
            do
            {
                id = rand.Next(9999);
            } while (GetUserById(id) != null);
            return id;
        }

        /// <summary>
        /// Returns a user from allUsers list with ID and password validation.
        /// </summary>
        private User ValidateUser()
        {
            Console.WriteLine("What is your account ID?");

            User user;
            do
            {
                int id = ReadInt();

                user = GetUserById(id);
                if (user == null)
                {
                    Console.WriteLine("An account with that ID does not exist. Try again.");
                }
            } while (user == null);

            Console.WriteLine("Account found! Insert your password.");

            ValidatePassword(user);
            return user;
        }

        /// <summary>
        /// Keeps asking for a password until a correct one is typed.
        /// </summary>
        private void ValidatePassword(User user)
        {
            string password;
            do
            {
                password = Console.ReadLine();
                if (user.Password != password)
                {
                    Console.WriteLine("Wrong password, try typing it again.");
                }
                else
                {
                    Console.WriteLine($"Correct password. We validated you as {user.Name}.");
                    break;
                }
            } while (user.Password != password);
        }
    }
}
