## Homework 2: Buutti Banking CLI

Please return the file as a .zip package to Johannes:
1. Right click the project root folder -> Send to -> .zip file
2. Rename the file h2_firstname_lastname.zip (for example: h2_johannes_kantola.zip)
3. Right click Johannes K in Discord -> Message -> Drag the .zip file into the message box

**Deadline: Friday 21.8.2020 9am**

This is a really common exercise that many programmers do at the beginning of their career. The reason it's so common is because it teaches to use many different types of variables and operations. Remember the DRY rule - Don't repeat yourself! If you find yourself copy-pasting code, you should instead wrap it inside a method.

### H2.1 Welcome to the banking system

Even inside the terminal, UI is crucial part of an application. Below you’ll see what your application should say when you run it the first time.

    $ Welcome to Buutti banking CLI!
    $ Hint: You can get help with the commands by typing "help".

After printing the text above, the application should wait for user input. Input consists of multiple options, so instead of overwhelming ourselves, let's define the behavior for the help command first.

### H2.2 Help and quit

If the user enters the `help` command, the text below should be printed out (without the dollar signs).

    $ Here’s a list of commands you can use!
    $ 
    $ help                Opens this dialog.
    $ quit                Quits the program.
    $ 
    $ Account actions
    $ create_account      Opens a dialog for creating an account.
    $ does_account_exist  Opens a dialog for checking if an account exists.
    $ account_balance     Opens a dialog for logging in and prints the account balance.
    $ 
    $ Fund actions
    $ withdraw_funds      Opens a dialog for withdrawing funds.
    $ deposit_funds       Opens a dialog for depositing funds.
    $ transfer_funds      Opens a dialog for transferring funds to another account.

The rest of the exercise consists of implementing all the features listed here. After running each command, the program should return to the beginning, where the user can input more commands.
Before jumping to more complex command chains, implement the `quit` command to exit the program.

## Accounts

### H2.3 Create an account

The `create_account` command starts a dialog sequence where the user is asked multiple questions. You should store the user's answers in their respective variables---implementation is up to you. Below is an example dialog. (Lines starting with `$` are printed by the program, `>` denotes user input.)

    $ Creating a new user account!
    $ Insert your name.
    > Rene Orosz
    $ Hello, Rene Orosz! It’s great to have you as a client.
    $ How much is your initial deposit? (The minimum is 10€)
    > 8
    $ Unfortunately we can’t open an account for such a small account. Do you have any more cash with you?
    > 12
    $ Great, Rene Orosz! You now have an account with a balance of 12€.
    $ We’re happy to have you as a customer, and we want to ensure that your money is safe with us. 
    $ Give us a password, which gives only you the access to your account.
    > hunter12
    $ Your account is now created.
    $ Account id: 2035
    $ Store your account ID in a safe place.

The data you collect from the user should be stored into a new User object akin to this one:

    User user = new User ();
    user.Name = name;
    user.Password = password;
    user.Id = id;
    user.Balance = balance;

The User class should support these properties.

Generate the ID within the program---it should be unique for all the users (EXTRA: not just integers 1, 2, 3, ... for consecutive new users).

User data objects should be stored in a list of User objects, called `allUsers`.

### H2.4 Does an account exist

The `does_account_exist` command starts a dialog sequence as well. Find if the account in question is stored in the `allUsers` list.

    $ Checking if an account exists!
    $ Enter the account ID whose existence you want to verify.
    > 69420
    $ An account with that ID does not exist. Try again.
    > 2035
    $ This account exists.
    
### H2.5 Account balance

The `account_balance` command starts a dialog sequence as well. User is asked for an account ID and a password. Given a correct ID-password pair, the account balance is printed.

    $ Checking your account balance!
    $ What is your account ID?
    > 69420
    $ An account with that ID does not exist. Try again.
    > 2035
    $ Account found! Insert your password.
    > hunetr12
    $ Wrong password, try typing it again.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ Your account balance is 12€.

## Funds

### H2.6 Withdraw funds

The `withdraw_funds` command starts a dialog sequence as well. Reuse elements from the previous parts for checking if an account exists and logging in.

    $ Withdrawing cash!
    $ What is your account ID?
    > 69420
    $ An account with that ID does not exist. Try again.
    > 2035
    $ Account found! Insert your password.
    > hunetr12
    $ Wrong password, try typing it again.
    > hunter12.
    $ Correct password. We validated you as Rene Orozzz.
    $ How much money do you want to withdraw? (Current balance: 12€)
    > 13
    $ Unfortunately you don’t have the balance for that. Try a smaller amount.
    > 11
    $ Withdrawing a cash sum of 11€. Your account balance is now 1€.

Find the correct account from the `allUsers` list, validate the user and update the account balance. Don't allow the user to exceed their balance in the withdrawal.

### H2.7 Deposit funds

The `deposit_funds` command starts a dialog sequence as well, very similarly to `withdraw_funds`.

    $ Depositing cash!
    $ What is your account ID?
    > 2035
    $ Account found! Insert your password.
    > hunter12.
    $ Correct password. We validated you as Rene Orozzz.
    $ How much money do you want to deposit? (Current balance: 12€)    
    > 250
    $ Depositing 250€. Your account balance is now 262€.
    
### H2.8 Transfer funds

The `transfer_funds` command starts a dialog sequence as well.

    $ Transferring funds!
    $ What is your account ID?
    > 2035
    $ Account found! Insert your password.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ How much money do you want to transfer? (Current balance: 262€)
    > 200
    $ Which account ID do you want to transfer these funds to?
    > 666
    $ An account with that ID does not exist. Try again.
    > 90570
    > Sending 11€ from account ID 2035 to account ID 90570.

## Extra

### H2.9 Request funds

Add a new command, `request_funds`, to the program. The `request_funds` command starts a dialog sequence as well.

    $ Requesting funds!
    $ What is your account ID?
    > 2035
    $ Account found! Insert your password.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ Which account ID do you request funds from?
    > 69420
    $ An account with that ID does not exist. Try again.
    > 90570
    $ Account found. How much money do you want to request?
    > 500
    $ Requesting 500€ from the user with ID 90570.

All the fund requests should be stored in a fundRequests list. It should contain the IDs of the requestee and the requester, and the requested amount.

### H2.10 Fund requests

Add a new command, `fund_requests`, to the program. The `fund_request` command lists the fund requests for your account.

    $ Listing fund requests!
    $ What is your account ID?
    > 2035
    $ Account found! Insert your password.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ Listing all the requests for your account!
    $  - 420€ for the user 69420.
    $  - 69€ for the user 69420.
    $  - 2.60€ for the user 90570.

### H2.11 Accept fund request

Add a new command, `accept_fund_request`, to the program. The `accept_fund_request` command is somewhat self-explanatory and expands the previous command.

    $ Accepting fund requests!
    $ What is your account ID?
    > 2035
    $ Account found! Insert your password.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ Listing all the requests for your account!
    $ 1.   420€ for the user 69420.
    $ 2.    69€ for the user 69420.
    $ 3.     2€ for the user 90570.
    $ Your account balance is 262€. Which fund request would you like to accept?
    > 1
    $ You do not have funds to accept this request.
    > 3
    $ Accepting fund request 2€ for the user 90570. 
    $ Transferring 2€ to account ID 90570.
    $ Your account balance is now 260€.

Remember to update the `fundRequests` list and balance of the both accounts.

### H2.12 Yes or no

Implement a yes/no question, and add it as a confirmation to important actions, e.g. for accepting the fund request.

    $ Are you sure?
    > no
    $ Terminating current action.

    $ Are you sure?
    > yes

### H2.13 Log in

Implement a `log_in` command that asks for a username and a password. Store the logged in user ID in a `loggedUser` variable. After logging in, other commands should validate the user automatically, and the `log_in` command is unavailable; instead, there should be a `log_out` command available.

    $ Logging in!
    $ What is your account ID?
    > 69420
    $ An account with that ID does not exist. Try again.
    > 2035
    $ Account found! Insert your password.
    > hunetr12
    $ Wrong password, try typing it again.
    > hunter12.
    $ Correct password. We validated you as Rene Orosz.
    $ You are now logged in.
