﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Week_5_Lecture_Assignments.Controllers
{
    // 5.2.
    // Routes to all methods start with "api/"
    [ApiController]
    [Route("api")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        // 5.2.
        // Route to this method is "api/weatherforecast"
        [HttpGet("[controller]")]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-30, 30),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        // 5.3.1
        // Route to this method is "api"
        [HttpGet]
        public List<string> GetStrings()
        {
            return new List<string> { "Foo", "Bar", "Blaa" };
        }

        // 5.3.2
        // Route to this method is "api/numberlist/{k}"
        [HttpGet("numberlist/{k:int}")]
        public int[] GetNumberArray(int k)
        {
            int[] arr = new int[k];
            for (int i = 0; i < k; i++)
            {
                arr[i] = i + 1;
            }
            return arr;
        }

        // 5.5
        // Route to this method is "api/notes"
        [HttpGet("notes")]
        public List<Note> GetNotes() => 
            NoteRepository.Instance.GetNotes();
        
        // Route to this method is "api/notes"
        [HttpPost("notes")]
        public Note PostNote([FromBody] Note note)
        {
            Note newNote = NoteRepository.Instance.AddNote(note);
            return newNote;
        }
    }

    public class Note
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string Creator { get; set; }
    }
    public class NoteRepository
    {
        public static NoteRepository Instance { get; } = new NoteRepository();

        private List<Note> notes = new List<Note>
        {
            new Note{Id=0, Content="It was good day today.", Creator="Johannes Kantola"},
            new Note{Id=1, Content="Hello world", Creator="Essi Esimerkki"}
        };

        private int runningId;

        public NoteRepository()
        {
             runningId = notes.Count;
        }

        public List<Note> GetNotes() => notes;

        public Note AddNote(Note note)
        {
            Note newNote = new Note
            {
                Id = runningId,
                Content = note.Content,
                Creator = note.Creator
            };
            runningId++;
            notes.Add(newNote);
            return newNote;
        }
    }
}
