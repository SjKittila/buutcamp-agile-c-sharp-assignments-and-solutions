## Homework 5: Exercise Log API

Please return the file as a .zip package to Johannes:
1. Right click the project root folder -> Send to -> .zip file
2. Rename the file h5_firstname_lastname.zip (for example: h5_johannes_kantola.zip)
3. Right click Johannes K in Discord -> Message -> Drag the .zip file into the message box

**Deadline: Wednesday 16.9.2020 9am**

In this assignment you will create an API for logging exercise entries. Each entry contains information about the date, type and duration of the exercise, for example:

    {
        "id": 4,
        "date": "2020-01-22T00:00:00",
        "exerciseType": 2,
        "duration": 50
    }

The API has endpoints for retrieving the entries from a database, and adding new ones.

### H5.1. Setting things up

Create a new ASP.NET Core web app using the API template. Uncheck "Configure for HTTPS" before creating your project.

Delete the `WeatherForecast.cs` file. Edit the `WeatherForecastController.cs` file under the `Controllers` folder: rename the class to `EntriesController` (right-click the class name -> Rename -> *enter new name* -> Apply). After that, change the `Get` method return type to `string` and return "Hello, World" from the method. You can delete the `Summaries` array, `_logger` variable and `EntriesController` constructor.
This will be the controller for all the HTTP requests for this API.

Create a new folder called `Models` and add a new class `Entry.cs` under it. This class will be the model for the entries in this API.

Finally, add a new folder and name it `Repositories`. Add a new class `EntriesRepository.cs` under it. This class will handle reading entries from / writing them to the database.

Open `launchSettings.json` under `Properties` in solution explorer. Change the value for `launchUrl` key to `entries`. If you run your application now, it should open the browser in `http://localhost:xxxxx/entries`, and show the "Hello world" text.

### H5.2. Creating the Model

Inside the `Entry` class, create a new `public enum Exercise { }` enum. Add some exercise types inside, like Cycling, Dancing, Jogging, Running...

Also add the following properties for the `Entry` class:

    int Id
    DateTime Date
    Exercise ExerciseType
    int Duration

### H5.3. Creating the Repository

Add the following line inside the `EntriesRepository` class:

`public static EntriesRepository Instance { get; } = new EntriesRepository();`

After that, add one public property `List<Entry> Entires`, and a constructor, which initializes the `Entries` list with some new `Entry` objects.

Change the type of the `Get` method in `EntriesController.cs` from `string` to `List<Entry>` and make it return `EntriesRepository.Instance.Entries`. Run the program. It should now show the list of entries you created in the `EntriesRepository` constructor.

Add the following methods to the `EntriesRepository` class:

    public List<Entry> GetEntries()
    public void AddEntry(Entry entry)

`GetEntries()` should return the `Entries` list.
`AddEntry()` should add the entry sent as a parameter to the `Entries` list.

### H5.4. GetEntry Method

Modify the `Get()` method in `EntriesController` again so that it returns `EntriesRepository.Instance.GetEntries()`.

Let's start adding new endpoints to our API.

Add the following method to the `EntriesController.cs` class:

    public Entry GetEntry(int id)

The method should return an `entry` that corresponds to the `id` sent as a parameter.

The URI to call this method should be
    
    http://localhost:xxxxx/entries/{id}

where {id} can be any integer. Test your endpoint with a browser or with Postman.

### H5.5. Post Method

Add the following method to the `EntriesController.cs` class:

    public string Post([FromBody] Entry entry)

The method should add the `entry` sent as a parameter to the `Entries` list in `EntriesRepository`, and return "Entry added successfully!".

The URI to call this method should be

    http://localhost:xxxxx/entries/

Test your endpoint with Postman. Remember to set the method to POST and set the following key and value in the *Header* tab

    KEY                     VALUE
    Content-Type            application/json

and set the content to JSON in *Body* tab. You can test for example with the following JSON:

    {
        "id": 4,
        "date": "2020-01-22T00:00:00",
        "exerciseType": 2,
        "duration": 50
    }

## Extra

### H5.6. GetEntriesByExercise Method

Add the following method to the `EntriesController.cs` class:

    public List<Entry> GetEntriesByExercise(string exercise)

The method should return all `entry` objects that correspond to the `exercise` sent as a parameter.

The URI to call this method should be

    http://localhost:xxxxx/entries/{exercise}

where {exercise} can be any string matching the values declared in the `Entry.Exercise` enum.

*Hint: To compare the `string exercise` with `enum Entry.Exercise`, you can for example use `Enum.TryParse()` method to parse the string into a Entry.Exercise enum.*
