## Group assignments, Week 5: Sales API

## Mandatory

### G5.1. API: Categories

Create a new ASP.NET API template project, change the default `WeatherForecastController` into `CategoryController` and have it return a list of all categories. You will need to add a new class declaration `Category`. Like in the Payment Terminal, it should have an `Id` and `Name`.

Verify that all categories are returned when sending a GET request to the endpoint `/categories`.


### G5.2. API: Products

Create `ProductController` for returning all of the products. You will need to create a class `Product`. The products should have the same properties as in Payment Terminal. Make sure that they are listed when visiting the appropriate endpoint.

### G5.3. Payment Terminal: API data

Update the payment terminal project so that the `CategoryRepository` and ``ProductRepository`` classes fetch the categories over HTTP from the created API endpoints.

**Hint 1:**

``HttpClient`` can be used for fetching the data, here’s an example: 
https://docs.microsoft.com/en-us/dotnet/api/system.net.http.httpclient

Use the ``GetStringAsync`` method in the comment to get the response as a ``string``. 


**Hint 2:**

Since Product has multiple constructors, you should specify the preferred one with [JsonConstructor] attribute.

## Recommended

### G5.4. POSTing data

Create a ``sales`` endpoint to the API and add a method to it for processing POST queries. The method should take a list of Product ids as the data and print them into the console.

Then, modify the Payment Terminal to send a POST query with the Pay button, with the shopping cart contents as the data.

## Extra (Challenging, added post-assignment)

### G5.5. Adding multiple cart items at once

Create a new window which opens when a product button is pressed, which asks the user for the number of items which should be added to the cart.

### G5.6. Remove items from cart

Add functionality to remove items from the shopping cart. The implementation is up to you; it could be, for example, a button in ShoppingCartRow.


