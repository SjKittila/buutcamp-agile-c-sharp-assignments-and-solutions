﻿using System;
using System.Collections.Generic;

namespace Lecture_Assignment_2_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome!");
            List<string> notes = new List<string>();

            while (true)
            {
                Console.Write("Insert your command: ");
                string input = Console.ReadLine();
                switch(input)
                {
                    case "quit":
                        break;

                    case "help":
                        Console.WriteLine("Avaliable commands:");
                        Console.WriteLine("-------------------");
                        Console.WriteLine("Command      Function");
                        Console.WriteLine("help         Displays this dialog");
                        Console.WriteLine("quit         Closes the program");
                        break;

                    case "add":
                        Console.WriteLine("Please write your note:");
                        string note = Console.ReadLine();
                        notes.Add(note);
                        break;

                    case "list":
                        foreach (string n in notes)
                        {
                            Console.WriteLine(n);
                        }
                        break;

                    case "remove":
                        for (int i = 0; i < notes.Count; i++)
                        {
                            Console.WriteLine($"{i}    {notes[i]}");
                        }

                        int index = int.Parse(Console.ReadLine());
                        notes.RemoveAt(index);
                        break;
                }
            }
        }
    }
}
