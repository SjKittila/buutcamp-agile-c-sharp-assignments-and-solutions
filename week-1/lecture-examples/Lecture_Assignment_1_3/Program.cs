﻿using System;

namespace Lecture_Assignment_1_3
{
    class Program
    {
        static void Main(string[] args)
        {
            string note = Console.ReadLine();
            if(note.Length < 30)
            {
                Console.WriteLine(DateTime.Now.ToString() + "\t" + note);
            }
            else
            {
                Console.WriteLine(DateTime.Now.ToString() + "\n" + note);
            }
        }
    }
}
