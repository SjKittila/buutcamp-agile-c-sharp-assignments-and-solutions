﻿using System;
using System.Collections.Generic;

namespace H1b_Example
{
    // REFERENCES
    //
    // Random class:
    // https://docs.microsoft.com/en-us/dotnet/api/system.random?view=netcore-3.1
    //
    // TryParse method:
    // https://docs.microsoft.com/en-us/dotnet/api/system.int32.tryparse?view=netcore-3.1

    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            int balance = 20;

            // ** Integrate your age checking here...*

            // Generate the slot wheels.
            // Could be used later on e.g. for visualizing the roll
            List<char> list1 = new List<char>
                { 'A', 'Q', 'J', 'Q', 'J', 'J', 'J', 'J', 'K', 'J', 'Q', 'K', 'Q' };
            List<char> list2 = new List<char>
                { 'A', 'J', 'K', 'Q', 'J', 'Q', 'Q', 'J', 'K', 'J', 'J', 'Q', 'J' };
            List<char> list3 = new List<char>
                { 'A', 'J', 'Q', 'J', 'Q', 'J', 'J', 'K', 'Q', 'J', 'K', 'Q', 'J' };

            // Main loop
            while (balance > 0)
            {
                int winMultiplier = 0;
                int lastWin;
                int bet;

                Console.WriteLine($"Current balance: {balance}\n");
                Console.Write("Your bet (1-10): ");

                // You could just use int.Parse here, but it will crash the program
                // if the user inputs an invalid value
                bool tryBet = int.TryParse(Console.ReadLine(), out bet);

                if (!tryBet || bet < 1 || bet > 10)
                {
                    Console.WriteLine("Invalid value.");
                    continue;
                }
                else if(bet > balance)
                {
                    Console.WriteLine("Not enough balance.");
                    continue;
                }

                // Get random elements from the lists
                char symbol1 = list1[random.Next(list1.Count)];
                char symbol2 = list2[random.Next(list2.Count)];
                char symbol3 = list3[random.Next(list3.Count)];

                Console.WriteLine($"{symbol1} {symbol2} {symbol3}");

                // Check if all symbols match
                if(symbol1 == symbol2 && symbol1 == symbol3)
                {
                    switch (symbol1)
                    {
                        case 'J':
                            winMultiplier = 2;
                            break;
                        case 'Q':
                            winMultiplier = 4;
                            break;
                        case 'K':
                            winMultiplier = 8;
                            break;
                        case 'A':
                            winMultiplier = 20;
                            break;
                    }
                }
                else
                {
                    winMultiplier = 0;
                }

                // Update balance
                lastWin = winMultiplier * bet;
                balance += lastWin - bet;

                Console.WriteLine($"Last win: {lastWin}");
            }

            Console.WriteLine("Out of balance.");
        }
    }
}
