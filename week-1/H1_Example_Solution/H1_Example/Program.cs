﻿using System;

namespace H1_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            int age;
            int yearOfBirth;

            Console.Write("Enter year of birth: ");
            yearOfBirth = int.Parse(Console.ReadLine());

            age = DateTime.Now.Year - yearOfBirth;
            Console.WriteLine($"Your age is {age}.");

            if(age < 0)
            {
                Console.WriteLine("Please enter a valid year.");
            }
            else if(age < 18)
            {
                Console.WriteLine("You are not old enough to use this application.");
            }
            else
            {
                Console.WriteLine("What was done to the VHS cassette after finishing a movie?");
                Console.WriteLine("1. The cassette was blown into.");
                Console.WriteLine("2. The cassette was flipped to reveal the rest of the content.");
                Console.WriteLine("3. The cassette was rewinded to begin at the start.");

                if(Console.ReadLine() == "3")
                {
                    Console.WriteLine("Welcome to the application!");
                }
                else
                {
                    Console.WriteLine("Nice try, kiddo..");
                }
            }
        }
    }
}
