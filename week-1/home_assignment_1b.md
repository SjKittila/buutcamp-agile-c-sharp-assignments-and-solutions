## Homework 1b: Slot Machine

Please return the file as a .zip package to Johannes:
1. Right click the project root folder -> Send to -> .zip file
2. Rename the file h1b_firstname_lastname.zip (for example: h1b_johannes_kantola.zip)
3. Right click Johannes K in Discord -> Message -> Drag the .zip file into the message box
**Deadline: 14.8.2020 9am**

### H1b.1. 

Create a .NET C# console application that draws symbols from three lists. Each list contains a certain number of symbols A, K, Q and J, in a random order (you can make an algorithm which randomizes the lists or just manually initialize the lists with the symbols in a seemingly random order).

The player starts with a balance of 20. Each time the player presses enter, the balance is decreased by one, and a symbol from each list is randomly selected. If all the symbols match, the player wins the amount specified in the table below:

| Symbol | No. of symbols in list | Win |
|--------|------------------------|-----|
| A      | 1                      | 20  |
| K      | 2                      | 8   |
| Q      | 4                      | 4   |
| J      | 6                      | 2   |

After each draw, the drawn symbols, the last win and current balance are printed to the console:

![Printout](imgs/printout.jpg)

### H1b.2. (Extra) 

Add a bet multiplier. The user can choose a bet of 1-10 credits each draw. The wins are adjusted accordingly (max win = 10 * 20 = 200).

### H1b.3. (Extra)

Integrate the age verification from home assignment 1 to the beginning of this program.