## Homework 1: Age Verification

Please return the file as a .zip package to Johannes:
1. Right click the project root folder -> Send to -> .zip file
2. Rename the file h1_firstname_lastname.zip (for example: h1_johannes_kantola.zip)
3. Right click Johannes K in Discord -> Message -> Drag the .zip file into the message box

**Deadline: 12.8.2020 9am**

### H1.1. 

Create a new .NET C# console application, which asks the users year of birth and calculates the age (get current year with DateTime.Now.Year). If the user is less than 18 years old, the program outputs

    Your age is *insert age here*.
    You are not old enough to use this application.

If the users age is 18 or older, the program outputs

    Your age is *insert age here*.
    Welcome to the application!

### H1.2.

Modify the application so that if the user is old enough, the application outputs a verification question with three options:

    What was done to the VHS cassette after finishing a movie?
    1. The cassette was blown into.
    2. The cassette was flipped to reveal the rest of the content.
    3. The cassette was rewinded to begin at the start.

If the user answers the question correctly, the program outputs

    Welcome to the application!

Otherwise, the program outputs

	Nice try, kiddo..

You can come up with your own verification questions and answers!
