## Group Assignment 1: Trying Out Git

### G1.1

The aim of this assignment is to get a little practice with git within your group. Choose one of the group members to be the project maintainer. You should collaborate closely so that everyone knows what’s going on.

**Maintainer:**
Create a new project in GitLab with a project name of “Group X Git Practice”, where X is your group letter. Check the box in Initialize repository with a README this time, so that the repository can be cloned into. In the panel in the left, select _Members_ and add Developer permissions to your group mates.  

**Everyone:**
All group members should now clone the repository into their local machines with git clone, as shown in the lecture. 

### G1.2

**Maintainer:**
Start out by creating the .gitignore file. Create the file as shown in the lecture and use a template for Visual Studio, for example, this one: 
https://github.com/github/gitignore/blob/master/VisualStudio.gitignore
(you can just copy the text and paste it into your .gitignore file).

Add, commit and push your changes into the remote master.

**Everyone else:**
Pull the changes into your local machines with git pull. Make sure the .gitignore file shows up in your project folder.

### G1.3

**Maintainer:**
Create a new console application in your git folder. Add, commit and push your changes into the remote master.

**Everyone else:**
Pull the changes into your local machines with git pull. Open the project solution in Visual Studio.

### G1.4

**Everyone:**
Create your own branch, and checkout into it. The branches could be named, for example, after your name.

Change the default “Hello World” output into “This text was written by *your name*”.

Add, commit and push your changes into GitLab with git push --set-upstream origin *your-branch-name*.

### G1.5

**Maintainer:**
Try merging all the individual branches into master. Resolve any merge conflicts. In the end, you should have a program, that executes the code made by all of your group members:

    This text was written by Johannes Kantola
    This text was written by Rene Orosz
    This text was written by Erkki Esimerkki




