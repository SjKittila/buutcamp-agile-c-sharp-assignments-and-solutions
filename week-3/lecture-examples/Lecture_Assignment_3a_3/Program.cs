﻿using System;
using System.Collections.Generic;

namespace Lecture_Assignment_3a_3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Shape> shapes = new List<Shape>
            {
                new Square(1.5, 1.5),
                new Triangle(1.5, 1.5),
                new Circle(1.5)
            };

            shapes.Sort();

            foreach(Shape shape in shapes)
                Console.WriteLine(shape.Area);
        }
    }
    class Shape : IComparable
    {
        public double Area { get; set; }
        public int CompareTo(object obj)
        {
            Shape otherShape = (Shape)obj;
            if (Area > otherShape.Area) return 1;
            else if (Area < otherShape.Area) return -1;
            else return 0;
            // or...
            // return Area.CompareTo(otherShape.Area);

        }
    }
    class Square : Shape
    {
        public Square(double width, double height)
        {
            Area = width * height;
        }
    }
    class Triangle : Shape
    {
        public Triangle(double width, double height)
        {
            Area = (width * height) / 2;
        }
    }
    class Circle : Shape
    {
        public Circle(double radius)
        {
            Area = Math.Pow(radius, 2) * Math.PI;
        }
    }
}
