﻿using System;
using System.Collections.Concurrent;

namespace Lecture_Example_3b_1
{
    delegate void PlantActionDelegate();
    class Program
    {
        static void Main(string[] args)
        {
            PlantActionDelegate plantActions = delegate { };

            while (true)
            {
                string command = Console.ReadLine();
                switch (command)
                {
                    case "ReleaseWater":
                        plantActions += ReleaseWater;
                        break;
                    case "ReleaseFertilizer":
                        plantActions += ReleaseFertilizer;
                        break;
                    case "IncreaseTemperature":
                        plantActions += IncreaseTemperature;
                        break;
                    case "run":
                        plantActions();
                        break;
                }
            }
        }

        static void ReleaseWater()
        {
            Console.WriteLine("Releasing water...");
        }
        static void ReleaseFertilizer()
        {
            Console.WriteLine("Release fertilizer...");
        }
        static void IncreaseTemperature()
        {
            Console.WriteLine("Increasing temperature...");
        }
    }
}
