﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Lecture_Assignment_3c_1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> text = new List<string>();

            while (true)
            {
                Console.WriteLine("Please enter a word.");
                string input = Console.ReadLine();

                if(input.Length > 0)
                {
                    text.Add(input);
                }
                else
                {
                    Console.WriteLine("Please enter your path.");
                    string path = Console.ReadLine();

                    if (File.Exists(path))
                    {
                        File.Delete(path);
                    }
                    File.WriteAllLines(path, text);
                    
                }
            }
        }
    }
}
