﻿using System;
using System.Collections.Generic;
using System.Dynamic;

namespace BuutCampLectureExamplesWeek3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Message> allMessages = new List<Message>();
            while (true)
            {
                string message = Console.ReadLine();
                Message newMessage = new Message(message);
                allMessages.Add(newMessage);
                Console.WriteLine("Last message: " + Message.LastMessage);
                Console.WriteLine("Total messages: " + Message.TotalMessages);
            }
        }
    }

    class Message
    {
        public static int TotalMessages { get; set; }
        public static string LastMessage { get; set; }
        public string MessageText { get; set; }

        public Message(string message)
        {
            TotalMessages++;
            LastMessage = message;
            MessageText = message;
        }
    }
}
