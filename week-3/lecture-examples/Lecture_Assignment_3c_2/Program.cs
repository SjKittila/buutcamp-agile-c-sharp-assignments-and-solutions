﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Lecture_Assignment_3c_2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Note> allNotes = new List<Note>();
            string path = @"C:\Users\OMISTAJA\source\repos\BuutCampLectureExamplesWeek3\Lecture_Assignment_3c_2\notes.json";
            int id = 0;
            while (true)
            {
                Console.WriteLine("Note:");
                string note = Console.ReadLine();
                id++;

                Note newNote = new Note
                {
                    Id = id,
                    TimeStamp = DateTime.Now,
                    Text = note
                };

                allNotes.Add(newNote);
                string jsonNotes = JsonConvert.SerializeObject(allNotes);
                File.WriteAllText(path, jsonNotes);
            }
        }
    }
    class Note
    {
        public int Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Text { get; set; }
    }
}
