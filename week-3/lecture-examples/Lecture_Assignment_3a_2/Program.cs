﻿using System;
using System.Collections.Generic;

namespace Lecture_Assignment_3a_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product1 = new Product { InfoText = "This is a great product." };
            Category category1 = new Category { InfoText = "Books, magazines etc." };

            List<IInfo> allInfo = new List<IInfo> { product1, category1 };

            while (true)
            {
                Console.ReadLine();
                foreach(IInfo info in allInfo)
                {
                    info.PrintInfo();
                }
            }
        }
    }

    interface IInfo
    {
        public string InfoText { get; set; }
        public void PrintInfo();
    }

    class Product : IInfo
    {
        public string InfoText { get; set; }

        public void PrintInfo()
        {
            Console.WriteLine("-- Product --");
            Console.WriteLine(InfoText);
        }
    }
    class Category : IInfo
    {
        public string InfoText { get ; set ; }

        public void PrintInfo()
        {
            Console.WriteLine("-- Category --");
            Console.WriteLine(InfoText);
            
        }
    }
}
