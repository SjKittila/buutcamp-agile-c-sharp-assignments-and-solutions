﻿using H3_Example_Solution.Controllers;
using H3_Example_Solution.Entities;
using System;
using System.Collections.Generic;

namespace H3_Example_Solution
{
    class Program
    {
        static void Main(string[] args)
        {
            App app = new App();
            app.Run();
        }
    }
}
