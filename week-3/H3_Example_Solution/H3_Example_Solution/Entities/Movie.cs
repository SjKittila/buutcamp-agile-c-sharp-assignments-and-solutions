﻿using System;
using System.Collections.Generic;
using System.Text;

namespace H3_Example_Solution.Entities
{
    class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Length { get; set; }
    }
}
