﻿using H3_Example_Solution.Controllers;
using H3_Example_Solution.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace H3_Example_Solution
{
    class App
    {
        MovieController mc = new MovieController();

        public void Run()
        {
            mc.AddDummyMovies();
            List<Movie> movies = mc.GetMovies();
            foreach(Movie movie in movies)
            {
                Console.WriteLine(movie.Name);
            }
        }
    }
}
