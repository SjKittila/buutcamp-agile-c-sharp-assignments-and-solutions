﻿using H3_Example_Solution.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using System.Linq;

namespace H3_Example_Solution.Controllers
{
    class MovieController
    {
        string path = @"C:\Users\OMISTAJA\source\Malliratkaisut\H3_Example_Solution\db.json";

        public List<Movie> GetMovies()
        {
            return JsonConvert.DeserializeObject<List<Movie>>(File.ReadAllText(path));
        }

        public void AddMovie(string name, string description, int length)
        {
            List<Movie> movies = GetMovies();

            Movie newMovie = new Movie
            {
                Id = movies.Count,
                Name = name,
                Description = description,
                Length = length
            };

            movies.Add(newMovie);
            UpdateDatabase(movies);
        }

        public void AddDummyMovies()
        {
            List<Movie> movies = new List<Movie>()
            {
                new Movie
                {
                    Id = 0, 
                    Name = "Terminator", 
                    Description = "Lorem ipsum dolor sit amet",
                    Length = 100
                },
                new Movie
                {
                    Id = 1, 
                    Name = "Happy Porter", 
                    Description = "Opsum silor dot ametopsum silor dot amet",
                    Length = 222
                },
                new Movie
                {
                    Id = 2, 
                    Name = "Lord Of the Flies: Two Ant Towers", 
                    Description = "Opsum silor dot ametopsum silor dot amet",
                    Length = 120
                },
            };
            UpdateDatabase(movies);
        }

        private void UpdateDatabase(List<Movie> movies)
        {
            string output = JsonConvert.SerializeObject(movies);
            File.WriteAllText(path, output);
        }
    }
}
