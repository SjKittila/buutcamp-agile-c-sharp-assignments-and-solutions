## Homework 3: Buutti Movie DataBase

Please return the file as a .zip package to Johannes:
1. Right click the project root folder -> Send to -> .zip file
2. Rename the file h3_firstname_lastname.zip (for example: h3_johannes_kantola.zip)
3. Right click Johannes K in Discord -> Message -> Drag the .zip file into the message box

**Deadline: Wednesday 2.9.2020 9am**

This assignment is an introduction to using entities and databases. In this assignment you will build a console application which manages a local JSON database of movies, with commands to get and add movies.


### H3.1 Required Classes

Create a new folder in your project by right-clicking your program (not solution) name in Solution Explorer -> *Add* -> *New Folder*, called `Entities`. Add a a new class inside of the folder called `Movie.cs`. Inside of the `Movie` class, create the following properties:

    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int Length { get; set; }

Create another folder in your project called `Controllers`. Add a new class inside of the folder called `MovieController.cs`. Inside of the `MovieController.cs` class, create the following variable:

    private string db = @"path/to/your/database";

...and the following methods:

    public List<Movie> GetMovies()
    public void AddMovie(string name, string description, int length)
    public void AddDummyMovies()

Remember to add `using YourSolutionName.Entities` at the top of the file to include the namespace containing the `Movie` class!

### H3.2 Initializing the Database

In `MovieController.cs`, set the value of `db` to your current project folder and a filename of `db.json`, for example: `@"C:\Users\Johannes\source\repos\H3\db.json"` (copy the path by right-clicking the project and selecting *Copy full path*).

In `AddDummyMovies()` method, generate some new `Movie` objects, with different values. Add them to a list of movies, serialize the list into a string with `JsonConvert.SerializeObject()`, and create a database file  with `File.WriteAllText()` using the `db` variable as the path. 

Implement the `GetMovies()` method so, that it deserializes the `db.json` file into a list of `Movie` objects and returns it (tip: if you follow the lecture example, this should only be two lines of code).

Test the methods from the Main method by creating a new instance of the `MovieController` class:

    MovieController mc = new MovieController();
    mc.AddDummyMovies();
    List<Movie> movies = mc.GetMovies();
    foreach(Movie movie in movies)
    {
        Console.WriteLine(movie.Name);
    }

This should print all the names of the movies you created in the `AddDummyMovies()` method. Also the new `db.json` file should have appeared in Solution Explorer, with the contents.

### H3.3 Adding a movie

Implement the `AddMovie()` method: it should first get the list of all movies with the `GetMovies()` method.

Then create a new variable `int id` and set the value as the lenght of all movies list.

Then create a new `Movie` instance with the `Name`, `Description` and `Length` values passed in the parameters, and `Id` is the newly created `id`.

Finally, add the new `Movie` object to the list of all movies, serialize the list and write the object on the `db.json` as in `AddDummyMovies()` before. Test the function in the Main method by calling it a couple of times with different values.