## Group assignments, Week 3

### Introduction

This week’s assignments revolve around a project with a rudimentary WPF GUI and the main goal is to fix the data sources so that the WPF framework can present the data. You should have access to the PaymentTerminal project before beginning the tasks.

You should go through the tasks as if they’re new features to a project. Meaning, that you should make new git branches for each of them.

## Mandatory

### G3.1. Payment Terminal: Categories

To begin with, let’s get some categories to show up.

Open up the `CategoryRepository.cs` file and add few `Category` objects to the list that gets returned. Drinks, Food and the like (come up with the theme of your shop with your group, it can be anything as long as there is at least 3 categories for your products).

If you run the program now, you should see empty buttons in the top bar.

The reason why they’re empty is because the Category constructor doesn’t yet do anything with the constructor arguments. Open up the Category.cs file and have the constructor assign the values appropriately. After this, the new categories should be visible in the top bar.

**Hint:**

The `Category` constructor should be given an unique `id` and `name` when a new `Category` object is created. The `id` is used to filter between products of different categories.

### G3.2. Payment Terminal: Products

Now that we have some categories. It’s time to add some products: Open up the `Product.cs` file and fill in both of the `Product` constructors appropriately.

After that, open up the `ProductRepository.cs` and have the `Get` method return a list with some products. But this isn’t quite enough for the products to show up yet. This will be taken care of in the next task.

**Hint 1:**

The `Product` has two constructors, but only the `Product(id, categoryId, name, price)` is useful when creating new products. The `categoryId` should match one of the created categories.

**Hint 2:**

We are using `long` as the type of price. This is because floating point numbers can be troublesome with currencies, as they’re not exact. As an example, 0.3 - 0.1 can have the value of 0.19999999999999998.

With long, we can decide that 100 equals $1 and 1 equals $0.01, without having the problems that arise with floating point arithmetic.

### G3.3. Payment Terminal: ProductList

Open up the `ProductList.xaml.cs` (open the `.xaml` file and press F7 or use solution explorer).

This component, which inherits `UserControl` is responsible for keeping the product list up-to-date and is lacking the code to do so.

`UpdateProductList` is responsible for updating the ObservableCollection. The GetFilteredProducts method is responsible for filtering the products by the category they have.

Implement the `UpdateProductListmethods` as described in the comments.

**Hint 1:**

For reference, you can see how the `ObservableCollection<Category>` is done in the `CategoryBar.xaml.cs` file.

**Hint 2:**

Returning all products in `GetFilteredProducts` before implementing any filtering can be helpful if no Products get listed.

## Recommended

### G3.5. Payment Terminal: File I/O

Update the `Get` methods in `CategoryRepository.cs` and `ProductRepository.cs` to load the categories and products from text files. Both JSON and CSV are good formats for this, but you can invent your own way to format and load the data.

## Extra

### G3.6. Payment Terminal: WPF styling

Improve the look of the product buttons to show the product price as well.

After that, update the CategoryButtons to show the number of products in parentheses, like `Food (3)`.

Make the category buttons change size, depending on how long the category name is.
