## Speed Typer

In this task you will be creating a game, where the player has to write words as fast as possible. You can work in groups or individually.

1. Start out by creating a list of words (you can google for an existing one or write some words yourself for testing). Display a random word from the list every time the program is executed.

2. Expand your program so that after the word is printed, the player has to write the displayed word and press enter. Only after writing the word correctly, a new word is displayed.

3. Each time a new word is displayed, save the current time into a variable:

`DateTime startTime = DateTime.Now;`

After the word is written correctly, calculate the difference between current time and `startTime` and display the elapsed time with `yourCalculatedDifference.TotalMillisecons`.

4. Add a score system, where the player is given points for each word according to the following formula:

`wordScore = elapsedTime * wordLength`

Keep count of the total score and display it to the player.


If you still have time…

5. Continue expanding the application by adding a main menu. The main menu displays two game modes, from which the player can choose at the beginning of the game:

**Score Run**
The player is first asked how many words the game should last for.
The game keeps running until it has asked the number of words the player has specified before the game.
The score is calculated as before.

**Infinite mode**
Calculate the average Words Per Minute into a new variable which is updated and printed after every word.
The game keeps running infinitely (you can add some way for the player to go back to the main menu).


If you *still* have time...

6. Come up with your own game mode and add it to the game.