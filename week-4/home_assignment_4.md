## Homework 4: Expanding Buutti Movie DataBase

Please return the file as a .zip package to Johannes:
1. Right click the project root folder -> Send to -> .zip file
2. Rename the file h4_firstname_lastname.zip (for example: h4_johannes_kantola.zip)
3. Right click Johannes K in Discord -> Message -> Drag the .zip file into the message box

**Deadline: Wednesday 9.9.2020 9am**

In this assginment you will use LINQ to filter and handle data that is fetched from a database, and exception handling to ensure a continuous flow of execution.

Continue working on the Buutti Movie DataBase which you started in home assignment 3.

### H4.1. Get and Remove Movie

Use LINQ for the following tasks.

In `MovieController.cs`, add a new method `RemoveMovie(int id)` to remove a movie from the database by ID. Add another method `GetFilteredMovies(string keyword)` that returns a list of all movies which names contain the keyword. Remember to update `db.json` after each operation.

## Main Program

### H4.2. The Basics

Create a command line interface for navigating the database. If the user enters the `help` command, the text below should be printed out (without the dollar signs):

    $ Here’s a list of commands you can use!
    $ 
    $ help                Open this dialog.
    $ quit                Quit the program.
    $ 
    $ search              List movies by search term.
    $ add                 Add a new movie to the database.
    $ remove              Remove a movie from the database.

Start by implementing the `help` and `quit` commands.

### H4.3. Search

Use the methods you have created in `MovieController.cs` for the following tasks.

Implement the `search` command: after selecting the command, the user is asked for a search term. The number of movies found and the names of the movies are then printed. Below is an example dialog (lines starting with `$` are printed by the program, `>` denotes user input.):

    $ Enter your search term:
    > ar
    $ Found 2 movies with search term 'ar':
    $ Arn: The Knight Templar
    $ Harry Potter and the Philosophers Stone

### H4.4. Add

Implement the `add` command: after selecting the command, the user is asked for the name, description and length of a movie. Then, a new movie with those values is added to the database. Below is an example dialog:

    $ Adding a movie.
    $ Please enter the details for the movie!
    $ Name:
    > The Lord of the Rings: Two Towers
    $ Description:
    > Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    $ Length:
    > 180
    $ Movie 'The Lord of the Rings: Two Towers' added!

### H4.5. Remove

Implement the `remove` command: after selecting the command, the user is asked for an ID. The name of the movie with the corresponding ID, if found, is printed to the user, with a confirmation message. If the user accepts, the movie is removed from the database. Below is an example dialog:

    $ Removing a movie.
    $ Please enter the ID of the movie you want to remove:
    > 1
    $ Removing movie 'Arn: The Knight Templar'. Are you sure (y/n)?
    > y
    $ Movie removed from the database.

### H4.6. Exception Handling

Using *try-catch*, add exception handling to all the parts of the code where exceptions could occur: invalid path, invalid input types, etc. Use that to inform the user of the occurred errors instead of the program crashing.

## Extra

### H4.7 Configuration File

Some information should be stored in a file independent of the application code. In this exercise, the path to the `db.json` will be stored in an `App.config` file that can be written to without having to modify the code.

Open NuGet package manager, and search for `configurationmanager`. Select and install `System.Configuration.ConfigurationManager` for your project.

Right click your project in Solution Explorer and select *Add -> New Item...*. Search for `configuration`, select *Application Configuration File* and hit Add. Open the newly created `App.config` file and add the following between the `<configuration>` tags (replace the path with what you have used for your `db.json` project previously):

    <appSettings>
        <add key="db" value="C:\Path\To\Your\JSON\db.json"/>
    </appSettings>

You can now use `ConfigurationManager.AppSettings.Get("db")` in your code to access the path. Replace all lines in your code, where the path is hard-coded, with this method.