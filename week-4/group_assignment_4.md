## Group assignments, Week 4

## Mandatory

### G4.1. Payment Terminal: Improvements

Go over the existing code and update loops to use LINQ when possible.

If the project uses files as the source of categories and products, add exception handling for cases when the file(s) do not exist.

If `ProductRepository.Find` method isn’t yet implemented, do so now because it will be required soon.

### G4.2. Payment Terminal: Shopping Cart Items

We want the product buttons to add products to the shopping cart element shown on the right side of the app.

Since customers can buy multiple of each product type, we can track the items and their counts with `CartItem` objects.

Implement the methods in the CartItem.cs file as described in the comments.

**Hint:**

What does it mean for two `CartItem` objects to be equal? In this program, the equality will be checked only when checking if the CartItem is already in the shopping cart. This is done so that we can either add the new item into the cart, or increment the count for an existing one.

Since the `CartItem.Id` should be unique between different products (as it’s inherited from Product), checking the ID will be enough.

### G4.3. Payment Terminal: Shopping Cart

If you take a look at the `ProductButton.Button_Click` method in `ProductButton.xaml.cs`, you can see that it invokes the static member `event AddProductEvent`. This makes it so that any other component can register their own event handlers to take care of what happens when the event is triggered.

The `ShoppingCart` constructor in `ShoppingCart.xaml.cs` has comments describing how to subscribe to the `AddProductEvent` event.

Implement the method bodies for the constructor and the `AddItem` method as described in the comments.

**Hint:**

The `ShoppingCart` class has two member variables containing `CartItems`: `CartItems` and `ItemCollection`, of which `CartItems` isn’t strictly necessary and could be ignored if you want to enumerate over the `ItemCollection` in `AddItem` method.

## Recommended

### G4.4. Payment Terminal: Prettier shopping cart

Improve the look of the shopping cart items.

## Extra

### G4.5. Payment Terminal: Total Sum in the Pay Button

Have the shopping cart total sum shown in the bottom-right Pay button. For example, like this:

    Pay 9.99€

**Hint:**

Creating a new public static event into the `ShoppingCart` class is a good first step. The other way is to create a `RoutedEvent`, as is done in the `CategoryButton.xaml.cs`.

