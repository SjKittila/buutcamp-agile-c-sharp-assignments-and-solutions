﻿using System;
using System.Collections.Generic;
using Homework_4_Expanding_Buutti_Movie_DataBase.Entities;

namespace Homework_4_Expanding_Buutti_Movie_DataBase
{
    class Program
    {
        static void Main(string[] args)
        {
            MovieController controller = new MovieController();
            UI ui = new UI(controller);
            ui.Start();
        }
    }
}
