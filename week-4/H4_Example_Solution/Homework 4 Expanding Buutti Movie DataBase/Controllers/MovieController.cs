﻿using System;
using System.Collections.Generic;
using System.IO;
using Homework_4_Expanding_Buutti_Movie_DataBase.Entities;
using Newtonsoft.Json;
using System.Linq;
using System.Configuration;

namespace Homework_4_Expanding_Buutti_Movie_DataBase
{
    class MovieController
    {
        //Because folder path is not checked in GetMovies(), db.json is created to root folder of the project
        private string db = ConfigurationManager.AppSettings.Get("db");

        public List<Movie> GetMovies()
        {
                //When adding the very first movie this method should create the list.
                //This is why folder path is not checked
                if (!File.Exists(db))
                {
                    return new List<Movie>();
                }
                else
                {
                    string json = File.ReadAllText(db);
                    List<Movie> allMovies = JsonConvert.DeserializeObject<List<Movie>>(json);
                    return allMovies;
                }
        }

        public void AddMovie(string name, string description, int length)
        {
            List<Movie> movies = GetMovies();
            Movie newMovie = new Movie(movies.Count, name, description, length);
            movies.Add(newMovie);
            UpdateDatabase(movies);
        }

        public void AddDummyMovies()
        {
            List<Movie> moviesList = GetMovies();
            Movie invisibleMan = new Movie(moviesList.Count, "The Invisible Man", "When Cecilia's abusive ex takes his own life and leaves her his fortune, she suspects his death was a hoax. As a series of coincidences turn lethal, Cecilia works to prove that she is being hunted by someone nobody can see.", 124);
            moviesList.Add(invisibleMan);
            Movie badBoys = new Movie(moviesList.Count, "Bad Boys for Life", "Miami detectives Mike Lowrey and Marcus Burnett must face off against a mother-and-son pair of drug lords who wreak vengeful havoc on their city.", 124);
            moviesList.Add(badBoys);
            Movie onward = new Movie(moviesList.Count, "Onward", "Two elven brothers embark on a quest to bring their father back for one day.", 102);
            moviesList.Add(onward);
            Movie emma = new Movie(moviesList.Count, "Emma", "In 1800s England, a well meaning but selfish young woman meddles in the love lives of her friends.", 124);
            moviesList.Add(emma);
            Movie neverRarely = new Movie(moviesList.Count, "Never Rarely Sometimes Always", "A pair of teenage girls in rural Pennsylvania travel to New York City to seek out medical help after an unintended pregnancy.", 101);
            moviesList.Add(neverRarely);

            UpdateDatabase(moviesList);
        }

        public void RemoveMovie(int id)
        {
                List<Movie> moviesList = GetMovies();

                moviesList.Remove(moviesList.First(movie => movie.Id == id));
                //Update id to correspond with list length to make sure movies created
                //after a removal will receive an unique id
                moviesList.ForEach(movie => movie.Id = moviesList.IndexOf(movie));
                UpdateDatabase(moviesList);
        }

        public List<Movie> GetFilteredMovies(string keyword)
        {
            List<Movie> moviesList = GetMovies();
            var filteredList = moviesList
                .Where(m => m.Name.ToLower().Contains(keyword.ToLower()))
                .ToList();
            return filteredList;
        }

        private void UpdateDatabase(List<Movie> moviesList)
        {
            string jsonString = JsonConvert.SerializeObject(moviesList);
            File.WriteAllText(db, jsonString);
        }
    }
}
