﻿using System;
using System.Collections.Generic;

namespace Lecture_Assignment_4c_1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> intList = GetPopulatedList(100, 5);
            foreach(int val in intList)
                Console.WriteLine(val);

            List<string> stringList = GetPopulatedList("Text", 3);
            foreach(string val in stringList)
                Console.WriteLine(val);
        }

        static List<T> GetPopulatedList<T>(T value, int length)
        {
            List<T> list = new List<T>();
            for (int i = 0; i < length; i++)
            {
                list.Add(value);
            }
            return list;
        }
    }
}
