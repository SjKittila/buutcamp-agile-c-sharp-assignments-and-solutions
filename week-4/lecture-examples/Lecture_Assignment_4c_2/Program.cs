﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Lecture_Assignment_4c_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] names = File.ReadAllLines
                (@"C:\Users\OMISTAJA\Buutti\bc-csharp\buutcamp-agile-csharp-assignments-and-solutions\week-4\BuutCampLectureAssignmentsWeek4\Lecture_Assignment_4c_2\names.txt");
            List<User> users = new List<User>();

            while (true)
            {
                Console.WriteLine("Enter your search term:");
                string searchTerm = Console.ReadLine();

                var searchResult = names
                    .Where(n => n.Contains(searchTerm))
                    .ToArray();

                Console.WriteLine
                    ($"Found {searchResult.Length} names with term {searchTerm}");

                if(searchResult.Length < 10)
                {
                    int id = 0;

                    foreach(string name in searchResult)
                    {
                        users.Add(new User { Id = id, Name = name });
                        id++;
                    }
                }

                var sortedUsers = users.OrderBy(u => u.Name.Length);
                foreach(User user in sortedUsers)
                    Console.WriteLine($"{user.Id}       {user.Name}");
            }
        }
    }
    class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
