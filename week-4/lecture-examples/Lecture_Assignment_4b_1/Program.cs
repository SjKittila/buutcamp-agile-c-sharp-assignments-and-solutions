﻿using System;
using System.IO;

namespace Lecture_Assignment_4b_1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                Console.WriteLine("Enter your target savings:");
                int target = int.Parse(Console.ReadLine());
                Console.WriteLine("Enter your monthly salary:");
                int salary = int.Parse(Console.ReadLine());

                int monthsUntilGoal = target / salary;

                Console.WriteLine("You will reach your goal in " + monthsUntilGoal);
                Console.WriteLine("Where do you want to save the result?");

                string folder = Console.ReadLine();
                string path = folder + @"\months_until_target.txt";
                File.WriteAllText(path, monthsUntilGoal.ToString());
            }
            catch(Exception e)
            {
                Console.WriteLine(e.GetType().Name);
                switch (e.GetType().Name)
                {
                    case "FormatException":
                        Console.WriteLine("Wrong input format.");
                        break;
                    case "DivideByZeroException":
                        Console.WriteLine("You will never reach your goal.");
                        break;
                    case "DirectoryNotFoundException":
                        Console.WriteLine("Invalid path.");
                        break;

                    // UnauthorizedAccessException
                    // OverflowException
                    // ...
                }
            }
        }
    }
}
