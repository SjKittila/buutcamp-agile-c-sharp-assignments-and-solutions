﻿using System;
using System.Threading;

namespace Lecture_Assignment_4b_2
{
    class Program
    {
        static void Main(string[] args)
        {
            DataLoader bigFileLoader = new DataLoader { WaitTime = 40 };
            DataLoader smallFileLoader = new DataLoader { WaitTime = 10 };
            ThreadStart loadingThreadStart = new ThreadStart(bigFileLoader.LoadData);
            ThreadStart loadingThreadStartSmall = new ThreadStart(smallFileLoader.LoadData);

            while(true)
            {
                Console.ReadKey();
                Thread loadingThread = new Thread(loadingThreadStart);
                Thread loadingThreadSmall = new Thread(loadingThreadStartSmall);
                loadingThread.Start();
                loadingThreadSmall.Start();
            }
        }

        public class DataLoader
        {
            public int WaitTime { get; set; }

            public void LoadData()
            {
                Console.WriteLine("Loading...");
                int progress = 0;

                while (progress <= 100)
                {
                    Thread.Sleep(WaitTime);
                    Console.WriteLine($"Progress: {progress}%");
                    progress++;
                }
                Console.WriteLine("Done.");
            }
        }
        
    }
}
