﻿using System;
using System.Threading.Tasks;

namespace Lecture_Assignment_4b_3
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Task loadingDataResult = LoadDataAsync();
                Console.ReadKey();
                // Console.WriteLine(loadingDataResult.Status);
            }
        }

        static async Task LoadDataAsync()
        {
            Console.WriteLine("Loading...");
            int progress = 0;

            while (progress <= 100)
            {
                await Task.Delay(10);
                Console.WriteLine($"Progress: {progress}%");
                progress++;
            }
            Console.WriteLine("Done.");
        }
    }
}
